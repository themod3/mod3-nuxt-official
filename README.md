## Overview

- [Deployment Status](#deployment-status)
- [Statement](#statement)
- [Services](#services)
- [Who We Are](#who-we-are)
- [Our Mission](#our-mission)
- [Team Members](#team-members)

# Deployment Status

[![Netlify Status](https://api.netlify.com/api/v1/badges/caa53437-b89b-42b2-8bac-8c30ea6afb76/deploy-status)](https://app.netlify.com/sites/themod3/deploys)

# Statement

From creating high fidelity interactive websites to providing your engineering staffing needs, we have you covered.

# Services

### Website Design:

**Description**
: Using psychology as our baseline, we create a workflow that redirects your customers to take action. Through hypothesis testing, we take the guesswork and subjectivity out of design decision.

Tags: `Website Optimization`

### Enterprise Systems

**Description**
: Your business has needs. Whether it's keeping track of customers or automated work order creation, we provide custom solutions that tie all of your business operations together.

Tags: `ERP` `MRP` `CRM`

### Manufacturing Systems

**Description**
: We can take your job shop, batch manufacturing, or production line and create an automated or semi-automated workflow that boosts your production output and maximizes your product quality.

Tags: `Production` `Design` `Manufacturing` `Assembly` `DFMA`

### Modeling & Analysis

**Description**
: Offload your design, simulation, and data analytics workload with our network of consultants. From 3D modeling to machine learning, we have the team to help support your day-to-day needs.

Tags: `CAD` `CAM` `FEA` `CFD`

# Who We Are

We strive to deliver results-driven solutions that adapt to the ever-changing demands of industry. We aim to help you focus on your core value offerings by alleviating your day-to-day operational burdens.

# Our Mission

We are the skilled professionals you are looking for to help with your existing and future UX and operations needs. Our backgrounds in mechanical/aerospace engineering, physics, fabrication, and software development help us design holistic hardware and software systems that work seamlessly together. We are humans designing systems for humans.

# Team Members

Leonardo Ramirez:

**Description**
: Descriptions about dean here

Tags: `Aerospace` `Systems` `Fullstack` `Cloud` `Network` `DevOps` `Database`

Dean White:

**Description**
: Descriptions about dean here

Tags: `Manufacturing` `Project Mangement` `Design` `Modeling` `Simulation` `Mechatronics/Robotics` `Machine Vision`
