import { setLocalStorage } from '../util/localstorage';

const state = () => ({
  status: {
    isVisible: false,
    message: null,
  },

  isEnabled: {
    // -- IMPLEMENT IN FUTURE RELEASE --
    tidio: false,
    darkMode: true,
  },

  // -- IMPLEMENT IN FUTURE RELEASE --
  // show: false,
});

const mutations = {
  SET_STATUS: (state, status) => {
    state.status = status;
  },

  ENABLE_DARKMODE: (state, value) => {
    state.isEnabled.darkMode = value;
  },

  ENABLE_TIDIO: (state, value) => {
    state.isEnabled.tidio = value;
  },

  // -- IMPLEMENT IN FUTURE RELEASE --
  // SET_FOLD: (state, show) => {
  //   state.show = show;
  // },

  UPDATE_STATUS_VISIBILITY: (state, isVisible) => {
    state.status.isVisible = isVisible;
  },
};

const actions = {
  setStatus({ commit }, status) {
    commit('SET_STATUS', status);
  },

  persistData({ commit }, data) {
    switch (data.name) {
      case 'darkMode':
        commit('ENABLE_DARKMODE', data.value);
        setLocalStorage('darkMode', data.value);
        break;
      case 'tidio':
        commit('ENABLE_tidio', data.value);
        setLocalStorage('tidio', data.value);
        break;
    }
  },

  // -- IMPLEMENT IN FUTURE RELEASE --
  // setFold({ commit }, show) {
  //   commit('SET_FOLD', show);
  // },

  updateStatusVisibility({ commit }, isVisible) {
    commit('UPDATE_STATUS_VISIBILITY', isVisible);
  },
};

export default {
  state,
  mutations,
  actions,
};
