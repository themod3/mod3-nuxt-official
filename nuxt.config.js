export default {
  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
    // https://go.nuxtjs.dev/vuetify
    '@nuxtjs/vuetify',
    // https://github.com/juliomrqz/nuxt-optimized-images
    '@aceforth/nuxt-optimized-images',
    // https://pwa.nuxtjs.org/
    '@nuxtjs/pwa',
    // https://www.npmjs.com/package/nuxt-compress
    'nuxt-compress',
  ],

  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://github.com/nuxt-community/robots-module
    '@nuxtjs/robots',
    // https://github.com/Developmint/nuxt-webfontloader
    'nuxt-webfontloader',
  ],

  //Mode Confirguration
  ssr: false,

  //Deployment Target Configuration
  target: 'static',

  // Vuetify module configuration (https://go.nuxtjs.dev/config-vuetify)
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    treeShake: true,
    theme: {
      dark: true,
    },
    defaultAssets: {
      font: false,
    },
  },

  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: ['~/assets/mod3-theme.scss', '~/assets/mod3-theme.scss'],

  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {},

  robots: {
    /* module options */
  },

  pwa: {
    manifest: {
      display: 'fullscreen',
    },
    meta: {
      appleStatusBarStyle: 'black-translucent',
      ogHost: 'https://www.themod3.com',
      ogUrl: 'https://www.themod3.com',
      ogImage: {
        path: '/images/home/home_info_graphic-min.png',
        width: '1200',
        height: '630',
        type: 'image/png',
      },
      twitterCard: 'summary_large_image',
      twitterSite: '@cryptosynth2020',
      twitterCreator: '@cryptosynth2020',
    },
  },

  // Global page headers (https://go.nuxtjs.dev/config-head)
  head: {
    link: [
      {
        rel: 'preload',
        as: 'font',
        href: '/fonts/Modern.woff2',
        type: 'font/woff2',
        crossorigin: 'anonymous',
      },
      {
        rel: 'preload',
        as: 'font',
        href: '/fonts/Modern.woff',
        type: 'font/woff',
        crossorigin: 'anonymous',
      },
      {
        rel: 'preload',
        as: 'font',
        href: '/fonts/Modern.ttf',
        type: 'font/ttf',
        crossorigin: 'anonymous',
      },
    ],
    script: [
      {
        src: '//code.tidio.co/0hzducx8bt3ticm1irugvgb0ffoxufdz.js',
        async: true,
      },
    ],
  },

  webfontloader: {
    google: {
      families: ['JetBrains+Mono:wght@300&display=swap'],
    },
  },

  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: [{ src: '~/plugins/vueTyper.js', ssr: false }],

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: true,

  // Load Indicator Configuration
  loadingIndicator: {
    name: 'circle',
    color: 'orange',
    background: 'black',
  },
};
